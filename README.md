# Neo4j Browser

## Development setup
1. Clone this repo
1. Install yarn globally (not required but recommended): `npm install -g yarn`
1. Install project dependencies: `yarn`

### Development server
`yarn start` and point your web browser to `http://localhost:8080`.

### Testing
`yarn test` to run a single test run. A linter will run first.

`yarn dev` to have continous testing on every file change.

## Devtools
Download these two chrome extensions:
- [Redux devtools](https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd?hl=en)
- [React devtools](https://chrome.google.com/webstore/detail/react-developer-tools/fmkadmapgofadopljbjfkapdkoienihi?hl=en)

# CloudCoreo-Neo4j
CloudCoreo branded neo4j container

## How to build container
`docker build -t [name]:[tag] /path/to/directory/with/Dockerfile`

Example:  `docker build -t cloudcoreo-neo4j:latest .`

## How to run container
Running container on localhost

`docker run --rm -d --publish=7474:7474 --publish=7687:7687 --name [container-process-name] -e NEO4J_AUTH=neo4j/Password0 -e [docker-image-id]`

Example: `docker run --rm -d --publish=7474:7474 --publish=7687:7687 --name neo4j -e NEO4J_AUTH=neo4j/Password0  cloudcoreo-neo4j:latest`

Use directly from docker hub: `docker run --rm -d --publish=7474:7474 --publish=7687:7687 --name neo4j -e NEO4J_AUTH=neo4j/Password0 jacho/cloudcoreo-neo4j`

Then open `localhost:7474`
