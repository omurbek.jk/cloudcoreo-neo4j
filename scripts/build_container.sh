#!/bin/bash

set -x

CC_ENVIRONMENT="${CC_ENVIRONMENT:-dev}"
CC_NEO4J_IMAGE_NAME="${CC_NEO4J_IMAGE_NAME:-cloudcoreo-neo4j}"
CONATINER_VERSION="${CONTAINER_VERSION:-0}"
REGISTRY="${REGISTRY:-localhost:5000}"

dirname=$(dirname $0)
tagname="${REGISTRY}/${CC_NEO4J_IMAGE_NAME}${CC_ENVIRONMENT}"

(
cd "$dirname/.."
docker build -t "${tagname}:${CONTAINER_VERSION}" -t "${tagname}:latest" .

docker push "${tagname}:${CONTAINER_VERSION}"
docker push "${tagname}:latest"
)