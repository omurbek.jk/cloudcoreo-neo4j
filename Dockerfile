FROM neo4j:latest

RUN rm /var/lib/neo4j/data \
    && mkdir /var/lib/neo4j/data \
    && mv /data/* /var/lib/neo4j/data/

COPY ./conf/neo4j.conf  /var/lib/neo4j/conf/

COPY ./target/neo4j-browser-3.1.4.jar /var/lib/neo4j/lib/neo4j-browser-3.1.4.jar

EXPOSE 7474 7473 7687

ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["neo4j"]
